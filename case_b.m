%clear all
%load('data/data_base_parameters_BV_conc.mat')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%change data files here:
data_340_340=data_340_340_RH70_1atm;
data_350_350=data_350_350_RH70_1atm;
data_340_345=data_340_345_RH70_1atm;
data_345_340=data_345_340_RH70_1atm;
data_335_340=data_335_340_RH70_1atm;
data_340_335=data_340_335_RH70_1atm;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cude=data_340_340.iv(1,:);
x_id=data_340_340.X_ID{1};
IV_340_340=data_340_340.iv;
IV_350_350=data_350_350.iv;
IV_340_345=data_340_345.iv;
IV_345_340=data_345_340.iv;
IV_335_340=data_335_340.iv;
IV_340_335=data_340_335.iv;
for k=1:length(data_340_340_RH70_1atm.iv(1,:))
    profile_T_340(k,:)     = data_340_340.SOL{k}.y(1,:);
    profile_T_350(k,:)     = data_350_350.SOL{k}.y(1,:);
    profile_T_340_345(k,:) = data_340_345.SOL{k}.y(1,:);
    profile_T_345_340(k,:) = data_345_340.SOL{k}.y(1,:);
    profile_T_335_340(k,:) = data_335_340.SOL{k}.y(1,:);
    profile_T_340_335(k,:) = data_340_335.SOL{k}.y(1,:);
    
    profile_Jq_340(k,:)     = data_340_340.SOL{k}.y(4,:);
    profile_Jq_350(k,:)     = data_350_350.SOL{k}.y(4,:);
    profile_Jq_340_345(k,:) = data_340_345.SOL{k}.y(4,:);
    profile_Jq_345_340(k,:) = data_345_340.SOL{k}.y(4,:);
    profile_Jq_335_340(k,:) = data_335_340.SOL{k}.y(4,:);
    profile_Jq_340_335(k,:) = data_340_335.SOL{k}.y(4,:);
    
    profile_pot_340(k,:)     = data_340_340.SOL{k}.y(3,:);
    profile_pot_350(k,:)     = data_350_350.SOL{k}.y(3,:);
    profile_pot_340_345(k,:) = data_340_345.SOL{k}.y(3,:);
    profile_pot_345_340(k,:) = data_345_340.SOL{k}.y(3,:);
    profile_pot_335_340(k,:) = data_335_340.SOL{k}.y(3,:);
    profile_pot_340_335(k,:) = data_340_335.SOL{k}.y(3,:);
 
    pel_a_gdl_340(k,1)     = data_340_340.CONTR{k}(2,(x_id(2)+x_id(1))/2);
    pel_a_gdl_340_345(k,1) = data_340_345.CONTR{k}(2,(x_id(2)+x_id(1))/2);
    pel_a_gdl_345_340(k,1) = data_345_340.CONTR{k}(2,(x_id(2)+x_id(1))/2);
    
    pel_c_gdl_340(k,1)     = data_340_340.CONTR{k}(2,(x_id(6)+x_id(5))/2);
    pel_c_gdl_340_345(k,1) = data_340_345.CONTR{k}(2,(x_id(6)+x_id(5))/2);
    pel_c_gdl_345_340(k,1) = data_345_340.CONTR{k}(2,(x_id(6)+x_id(5))/2);
    
    pel_m_340(k,1)         = data_340_340.CONTR{k}(2,(x_id(4)+x_id(3))/2);
    pel_m_340_345(k,1)     = data_340_345.CONTR{k}(2,(x_id(4)+x_id(3))/2);
    pel_m_345_340(k,1)     = data_345_340.CONTR{k}(2,(x_id(4)+x_id(3))/2);
    
    fourier_a_gdl_340(k,1)     = data_340_340.CONTR{k}(3,(x_id(2)+x_id(1))/2);
    fourier_a_gdl_340_345(k,1) = data_340_345.CONTR{k}(3,(x_id(2)+x_id(1))/2);
    fourier_a_gdl_345_340(k,1) = data_345_340.CONTR{k}(3,(x_id(2)+x_id(1))/2);
    
    fourier_c_gdl_340(k,1)     = data_340_340.CONTR{k}(3,(x_id(6)+x_id(5))/2);
    fourier_c_gdl_340_345(k,1) = data_340_345.CONTR{k}(3,(x_id(6)+x_id(5))/2);
    fourier_c_gdl_345_340(k,1) = data_345_340.CONTR{k}(3,(x_id(6)+x_id(5))/2);
    
    fourier_m_340(k,1)         = data_340_340.CONTR{k}(3,(x_id(4)+x_id(3))/2);
    fourier_m_340_345(k,1)     = data_340_345.CONTR{k}(3,(x_id(4)+x_id(3))/2);
    fourier_m_345_340(k,1)     = data_345_340.CONTR{k}(3,(x_id(4)+x_id(3))/2);
    
    
    hof_a_gdl_340(k,1)     = data_340_340.CONTR{k}(1,(x_id(2)+x_id(1))/2);
    hof_a_gdl_340_345(k,1) = data_340_345.CONTR{k}(1,(x_id(2)+x_id(1))/2);
    hof_a_gdl_345_340(k,1) = data_345_340.CONTR{k}(1,(x_id(2)+x_id(1))/2);
    
    hof_c_gdl_340(k,1)     = data_340_340.CONTR{k}(1,(x_id(6)+x_id(5))/2);
    hof_c_gdl_340_345(k,1) = data_340_345.CONTR{k}(1,(x_id(6)+x_id(5))/2);
    hof_c_gdl_345_340(k,1) = data_345_340.CONTR{k}(1,(x_id(6)+x_id(5))/2);
    
    hof_m_340(k,1)         = data_340_340.CONTR{k}(1,(x_id(4)+x_id(3))/2);
    hof_m_340_345(k,1)     = data_340_345.CONTR{k}(1,(x_id(4)+x_id(3))/2);
    hof_m_345_340(k,1)     = data_345_340.CONTR{k}(1,(x_id(4)+x_id(3))/2);
    
    pel_a_gdl_350(k,1)     = data_350_350.CONTR{k}(2,(x_id(2)+x_id(1))/2);
    pel_a_gdl_335_340(k,1) = data_335_340.CONTR{k}(2,(x_id(2)+x_id(1))/2);
    pel_a_gdl_340_335(k,1) = data_340_335.CONTR{k}(2,(x_id(2)+x_id(1))/2);
    
    pel_c_gdl_350(k,1)     = data_350_350.CONTR{k}(2,(x_id(6)+x_id(5))/2);
    pel_c_gdl_335_340(k,1) = data_335_340.CONTR{k}(2,(x_id(6)+x_id(5))/2);
    pel_c_gdl_340_335(k,1) = data_340_335.CONTR{k}(2,(x_id(6)+x_id(5))/2);
    
    pel_m_350(k,1)     = data_350_350.CONTR{k}(2,(x_id(4)+x_id(3))/2);
    pel_m_335_340(k,1) = data_335_340.CONTR{k}(2,(x_id(4)+x_id(3))/2);
    pel_m_340_335(k,1) = data_340_335.CONTR{k}(2,(x_id(4)+x_id(3))/2);
    
    fourier_a_gdl_350(k,1)     = data_350_350.CONTR{k}(3,(x_id(2)+x_id(1))/2);
    fourier_a_gdl_335_340(k,1) = data_335_340.CONTR{k}(3,(x_id(2)+x_id(1))/2);
    fourier_a_gdl_340_335(k,1) = data_340_335.CONTR{k}(3,(x_id(2)+x_id(1))/2);
    
    fourier_c_gdl_350(k,1)     = data_350_350.CONTR{k}(3,(x_id(6)+x_id(5))/2);
    fourier_c_gdl_335_340(k,1) = data_335_340.CONTR{k}(3,(x_id(6)+x_id(5))/2);
    fourier_c_gdl_340_335(k,1) = data_340_335.CONTR{k}(3,(x_id(6)+x_id(5))/2);
    
    fourier_m_350(k,1)         = data_350_350.CONTR{k}(3,(x_id(4)+x_id(3))/2);
    fourier_m_335_340(k,1)     = data_335_340.CONTR{k}(3,(x_id(4)+x_id(3))/2);
    fourier_m_340_335(k,1)     = data_340_335.CONTR{k}(3,(x_id(4)+x_id(3))/2);
    
    
    hof_a_gdl_350(k,1)     = data_350_350.CONTR{k}(1,(x_id(2)+x_id(1))/2);
    hof_a_gdl_335_340(k,1) = data_335_340.CONTR{k}(1,(x_id(2)+x_id(1))/2);
    hof_a_gdl_340_335(k,1) = data_340_335.CONTR{k}(1,(x_id(2)+x_id(1))/2);
    
    hof_c_gdl_350(k,1)     = data_350_350.CONTR{k}(1,(x_id(6)+x_id(5))/2);
    hof_c_gdl_335_340(k,1) = data_335_340.CONTR{k}(1,(x_id(6)+x_id(5))/2);
    hof_c_gdl_340_335(k,1) = data_340_335.CONTR{k}(1,(x_id(6)+x_id(5))/2);
    
    hof_m_350(k,1)         = data_350_350.CONTR{k}(1,(x_id(4)+x_id(3))/2);
    hof_m_335_340(k,1)     = data_335_340.CONTR{k}(1,(x_id(4)+x_id(3))/2);
    hof_m_340_335(k,1)     = data_340_335.CONTR{k}(1,(x_id(4)+x_id(3))/2);
end
contributions_heat_340     = [pel_a_gdl_340(end,:),pel_m_340(end,:),pel_c_gdl_340(end,:),...
                              fourier_a_gdl_340(end,:),fourier_m_340(end,:),fourier_c_gdl_340(end,:),...
                              hof_a_gdl_340(end,:),hof_m_340(end,:),hof_c_gdl_340(end,:),...
                              profile_Jq_340(end,:),profile_Jq_340(end,:),profile_Jq_340(end,:)];
contributions_heat_340_345 = [pel_a_gdl_340_345(end,:),pel_m_340_345(end,:),pel_c_gdl_340_345(end,:),...
                              fourier_a_gdl_340_345(end,:),fourier_m_340_345(end,:),fourier_c_gdl_340_345(end,:),...
                              hof_a_gdl_340_345(end,:),hof_m_340_345(end,:),hof_c_gdl_340_345(end,:),...
                              profile_Jq_340_345(end,:),profile_Jq_340_345(end,:),profile_Jq_340_345(end,:)];
contributions_heat_345_340 = [pel_a_gdl_345_340(end,:),pel_m_345_340(end,:),pel_c_gdl_345_340(end,:),...
                              fourier_a_gdl_345_340(end,:),fourier_m_345_340(end,:),fourier_c_gdl_345_340(end,:),...
                              hof_a_gdl_345_340(end,:),hof_m_345_340(end,:),hof_c_gdl_345_340(end,:),...
                              profile_Jq_345_340(end,:),profile_Jq_345_340(end,:),profile_Jq_345_340(end,:)];
                          


contributions_heat_350     = [pel_a_gdl_350(end,:),pel_m_350(end,:),pel_c_gdl_350(end,:),...
                              fourier_a_gdl_350(end,:),fourier_m_350(end,:),fourier_c_gdl_350(end,:),...
                              hof_a_gdl_350(end,:),hof_m_350(end,:),hof_c_gdl_350(end,:),...
                              profile_Jq_350(end,:),profile_Jq_350(end,:),profile_Jq_350(end,:)];
contributions_heat_335_340 = [pel_a_gdl_335_340(end,:),pel_m_335_340(end,:),pel_c_gdl_335_340(end,:),...
                              fourier_a_gdl_335_340(end,:),fourier_m_335_340(end,:),fourier_c_gdl_335_340(end,:),...
                              hof_a_gdl_335_340(end,:),hof_m_335_340(end,:),hof_c_gdl_335_340(end,:),...
                              profile_Jq_335_340(end,:),profile_Jq_335_340(end,:),profile_Jq_335_340(end,:)];
contributions_heat_340_335 = [pel_a_gdl_340_335(end,:),pel_m_340_335(end,:),pel_c_gdl_340_335(end,:),...
                              fourier_a_gdl_340_335(end,:),fourier_m_340_335(end,:),fourier_c_gdl_340_335(end,:),...
                              hof_a_gdl_340_335(end,:),hof_m_340_335(end,:),hof_c_gdl_340_335(end,:),...
                              profile_Jq_340_335(end,:),profile_Jq_340_335(end,:),profile_Jq_340_335(end,:)];


T_peak_c(:,1)=profile_T_340(:,x_id(4)); 
T_peak_a(:,1)=profile_T_340(:,x_id(3));
T_peak_c(:,2)=profile_T_350(:,x_id(4)); 
T_peak_a(:,2)=profile_T_350(:,x_id(3));
T_peak_c(:,3)=profile_T_340_345(:,x_id(4)); 
T_peak_a(:,3)=profile_T_340_345(:,x_id(3));
T_peak_c(:,4)=profile_T_345_340(:,x_id(4)); 
T_peak_a(:,4)=profile_T_345_340(:,x_id(3));
T_peak_c(:,5)=profile_T_335_340(:,x_id(4)); 
T_peak_a(:,5)=profile_T_335_340(:,x_id(3));
T_peak_c(:,6)=profile_T_340_335(:,x_id(4)); 
T_peak_a(:,6)=profile_T_340_335(:,x_id(3));   

delta_T_a=round([T_peak_a(:,1)-profile_T_340(:,x_id(2)),T_peak_a(:,2)-profile_T_350(:,x_id(2)),T_peak_a(:,3)-profile_T_340_345(:,x_id(2)),...
           T_peak_a(:,4)-profile_T_345_340(:,x_id(2)),T_peak_a(:,5)-profile_T_335_340(:,x_id(2)),T_peak_a(:,6)-profile_T_340_335(:,x_id(2))],2);
delta_T_c=round([profile_T_340(:,x_id(5))-T_peak_c(:,1),profile_T_350(:,x_id(5))-T_peak_c(:,2),profile_T_340_345(:,x_id(5))-T_peak_c(:,3),...
           profile_T_345_340(:,x_id(5))-T_peak_c(:,4),profile_T_335_340(:,x_id(5))-T_peak_c(:,5),profile_T_340_335(:,x_id(5))-T_peak_c(:,6)],2);


pot_peak_c(:,1)=profile_pot_340(:,x_id(4)); 
pot_peak_a(:,1)=profile_pot_340(:,x_id(3));
pot_peak_c(:,2)=profile_pot_350(:,x_id(4)); 
pot_peak_a(:,2)=profile_pot_350(:,x_id(3));
pot_peak_c(:,3)=profile_pot_340_345(:,x_id(4)); 
pot_peak_a(:,3)=profile_pot_340_345(:,x_id(3));
pot_peak_c(:,4)=profile_pot_345_340(:,x_id(4)); 
pot_peak_a(:,4)=profile_pot_345_340(:,x_id(3));
pot_peak_c(:,5)=profile_pot_335_340(:,x_id(4)); 
pot_peak_a(:,5)=profile_pot_335_340(:,x_id(3));
pot_peak_c(:,6)=profile_pot_340_335(:,x_id(4)); 
pot_peak_a(:,6)=profile_pot_340_335(:,x_id(3)); 

pot_cell(:,1)=profile_pot_340(:,x_id(6)); 
pot_cell(:,2)=profile_pot_350(:,x_id(6)); 
pot_cell(:,3)=profile_pot_340_345(:,x_id(6)); 
pot_cell(:,4)=profile_pot_345_340(:,x_id(6)); 
pot_cell(:,5)=profile_pot_335_340(:,x_id(6)); 
pot_cell(:,6)=profile_pot_340_335(:,x_id(6)); 
      
       
delta_pot_a=round([pot_peak_a(:,1)-profile_pot_340(:,x_id(2)),pot_peak_a(:,2)-profile_pot_350(:,x_id(2)),pot_peak_a(:,3)-profile_pot_340_345(:,x_id(2)),...
           pot_peak_a(:,4)-profile_pot_345_340(:,x_id(2)),pot_peak_a(:,5)-profile_pot_335_340(:,x_id(2)),pot_peak_a(:,6)-profile_pot_340_335(:,x_id(2))],3);
delta_pot_c=round([profile_pot_340(:,x_id(5))-pot_peak_c(:,1),profile_pot_350(:,x_id(5))-pot_peak_c(:,2),profile_pot_340_345(:,x_id(5))-pot_peak_c(:,3),...
           profile_pot_345_340(:,x_id(5))-pot_peak_c(:,4),profile_pot_335_340(:,x_id(5))-pot_peak_c(:,5),profile_pot_340_335(:,x_id(5))-pot_peak_c(:,6)],3);
      
       
       
       
       
diff_T_peak_c(:,1)=abs(T_peak_c(:,1)-T_peak_c(:,3));
diff_T_peak_c(:,2)=abs(T_peak_c(:,1)-T_peak_c(:,4));
diff_T_peak_c(:,3)=abs(T_peak_c(:,2)-T_peak_c(:,5));
diff_T_peak_c(:,4)=abs(T_peak_c(:,2)-T_peak_c(:,6));
max_diff_T_peak_c=max(diff_T_peak_c);
min_diff_T_peak_c=min(diff_T_peak_c);

diff_T_peak_a(:,1)=abs(T_peak_a(:,1)-T_peak_a(:,3));
diff_T_peak_a(:,2)=abs(T_peak_a(:,1)-T_peak_a(:,4));
diff_T_peak_a(:,3)=abs(T_peak_a(:,2)-T_peak_a(:,5));
diff_T_peak_a(:,4)=abs(T_peak_a(:,2)-T_peak_a(:,6));
max_diff_T_peak_a=max(diff_T_peak_a);
min_diff_T_peak_a=min(diff_T_peak_a);

diff_pot_end(:,1)=profile_pot_340(end,x_id(5))-profile_pot_340_345(end,x_id(5));
diff_pot_end(:,2)=profile_pot_340(end,x_id(5))-profile_pot_345_340(end,x_id(5));
diff_pot_end(:,3)=profile_pot_350(end,x_id(5))-profile_pot_335_340(end,x_id(5));
diff_pot_end(:,4)=profile_pot_350(end,x_id(5))-profile_pot_340_335(end,x_id(5));



%%Plot IV curves for all cases
%Define here which current densities should be plotted
%% IV curve
figure(1)
hold on
plot(IV_340_340(1,:),IV_340_340(2,:),'k-','LineWidth',2)
plot(IV_350_350(1,:),IV_350_350(2,:),'b-','LineWidth',2)
plot(IV_340_345(1,:),IV_340_345(2,:),'k--','LineWidth',2)
plot(IV_345_340(1,:),IV_345_340(2,:),'k:','LineWidth',2)
plot(IV_335_340(1,:),IV_335_340(2,:),'r--','LineWidth',2)
plot(IV_340_335(1,:),IV_340_335(2,:),'r:','LineWidth',2)
hold off
legend('340 K', '350 K', '340/345 K','345/340 K','335/340 K', '340/335 K','Location','southwest')
xlabel('Current Density [A/m^2]') 
ylabel('Cell Potential [V]') 
set(gca,'FontSize',20)
set(gcf,'Position',[1000,205,1200,1000])

% create a new pair of axes inside current figure
axes('position',[.65 .175 .25 .25])
box on % put box around new pair of axes
hold on
plot(IV_340_340(1,100:end),IV_340_340(2,100:end),'k-','LineWidth',2)
plot(IV_350_350(1,100:end),IV_350_350(2,100:end),'b-','LineWidth',2)
plot(IV_340_345(1,100:end),IV_340_345(2,100:end),'k--','LineWidth',2)
plot(IV_345_340(1,100:end),IV_345_340(2,100:end),'k:','LineWidth',2)
plot(IV_335_340(1,100:end),IV_335_340(2,100:end),'r--','LineWidth',2)
plot(IV_340_335(1,100:end),IV_340_335(2,100:end),'r:','LineWidth',2)
hold off
% plot on new axes
axis tight
%%
figure(2)
hold on
plot(cude',profile_pot_340(:,288),'k-','LineWidth',2)
plot(cude',profile_pot_350(:,288),'r-','LineWidth',2)
hold off
legend('340 K, 1 atm', '350 K, 1 atm')
xlabel('Current Density [A/m^2]') 
ylabel('Cell Potential [V]') 
set(gca,'FontSize',20)
set(gcf,'Position',[1000,205,1200,1000])

%% Calculate difference in contributions at 15000 A/m^2 at the end of each homogenous layer, compared to 350/350K and 340/340K case
% Column 1: 2: 3: 4: 5: 6: 7: 8: 9:
% Row: 1: 340/345K 2: 345/340K 3: 335/340K 4: 340/335K Row 
diff(1,:)=round(100-contributions_heat_340.*100./contributions_heat_340_345,1);
diff(2,:)=round(100-contributions_heat_340.*100./contributions_heat_345_340,1);
diff(3,:)=round(100-contributions_heat_340.*100./contributions_heat_335_340,1);
diff(4,:)=round(100-contributions_heat_340.*100./contributions_heat_340_335,1);