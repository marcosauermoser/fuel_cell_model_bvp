clear all
close all
load('data/data_base_parameters_BV_conc.mat')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%change data files here:
data_340_340=data_340_340_RH70_1atm;
data_350_350=data_350_350_RH70_1atm;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cude       = data_340_340.iv(1,:);
x_id       = data_340_340.X_ID{1};
IV_340_340 = data_340_340.iv;
IV_350_350 = data_350_350.iv;
x_a_gdl    = data_340_340.SOL{1}.x(x_id(1):x_id(2));
x_m        = data_340_340.SOL{1}.x(x_id(3):x_id(4));
x_c_gdl    = data_340_340.SOL{1}.x(x_id(5):x_id(6));
for k=1:length(data_340_340_RH70_1atm.iv(1,:))
    profile_T_340(k,:)   = data_340_340.SOL{k}.y(1,:);
    profile_T_350(k,:)   = data_350_350.SOL{k}.y(1,:);
    
    profile_Jq_340(k,:)  = data_340_340.SOL{k}.y(4,:);
    profile_Jq_350(k,:)  = data_350_350.SOL{k}.y(4,:);
    
    profile_Jw_340(k,:)  = data_340_340.SOL{k}.y(7,:);
    profile_Jw_350(k,:)  = data_350_350.SOL{k}.y(7,:);

    %Contributions to heat flux
    pel_a_gdl_340(k,1)     = data_340_340.CONTR{k}(2,(x_id(2)+x_id(1))/2);
    pel_c_gdl_340(k,1)     = data_340_340.CONTR{k}(2,(x_id(6)+x_id(5))/2);
    pel_m_340(k,1)         = data_340_340.CONTR{k}(2,(x_id(4)+x_id(3))/2);
    
    fourier_a_gdl_340(k,1) = data_340_340.CONTR{k}(3,(x_id(2)+x_id(1))/2);
    fourier_c_gdl_340(k,1) = data_340_340.CONTR{k}(3,(x_id(6)+x_id(5))/2);
    fourier_m_340(k,1)     = data_340_340.CONTR{k}(3,(x_id(4)+x_id(3))/2);
   
    hof_a_gdl_340(k,1)     = data_340_340.CONTR{k}(1,(x_id(2)+x_id(1))/2);
    hof_c_gdl_340(k,1)     = data_340_340.CONTR{k}(1,(x_id(6)+x_id(5))/2);
    hof_m_340(k,1)         = data_340_340.CONTR{k}(1,(x_id(4)+x_id(3))/2);
    
    pel_a_gdl_350(k,1)     = data_350_350.CONTR{k}(2,(x_id(2)+x_id(1))/2);
    pel_c_gdl_350(k,1)     = data_350_350.CONTR{k}(2,(x_id(6)+x_id(5))/2);
    pel_m_350(k,1)         = data_350_350.CONTR{k}(2,(x_id(4)+x_id(3))/2);
    
    fourier_a_gdl_350(k,1) = data_350_350.CONTR{k}(3,(x_id(2)+x_id(1))/2);
    fourier_c_gdl_350(k,1) = data_350_350.CONTR{k}(3,(x_id(6)+x_id(5))/2);
    fourier_m_350(k,1)     = data_350_350.CONTR{k}(3,(x_id(4)+x_id(3))/2);
   
    hof_a_gdl_350(k,1)     = data_350_350.CONTR{k}(1,(x_id(2)+x_id(1))/2);
    hof_c_gdl_350(k,1)     = data_350_350.CONTR{k}(1,(x_id(6)+x_id(5))/2);
    hof_m_350(k,1)         = data_350_350.CONTR{k}(1,(x_id(4)+x_id(3))/2);
    %Contributions to water flux
    thermal_a_gdl_340(k,1)  = data_340_340.CONTR{k}(7,(x_id(2)+x_id(1))/2);
    thermal_c_gdl_340(k,1)  = data_340_340.CONTR{k}(7,(x_id(6)+x_id(5))/2);
    thermal_m_340(k,1)      = data_340_340.CONTR{k}(7,(x_id(4)+x_id(3))/2);
    
    ficks_a_gdl_340(k,1)    = data_340_340.CONTR{k}(8,(x_id(2)+x_id(1))/2);
    ficks_c_gdl_340(k,1)    = data_340_340.CONTR{k}(8,(x_id(6)+x_id(5))/2);
    ficks_m_340(k,1)        = data_340_340.CONTR{k}(8,(x_id(4)+x_id(3))/2);
       
    electro_a_gdl_340(k,1)  = data_340_340.CONTR{k}(9,(x_id(2)+x_id(1))/2);
    electro_c_gdl_340(k,1)  = data_340_340.CONTR{k}(9,(x_id(6)+x_id(5))/2);
    electro_m_340(k,1)      = data_340_340.CONTR{k}(9,(x_id(4)+x_id(3))/2);
    
    hydrogen_a_gdl_340(k,1) = data_340_340.CONTR{k}(10,(x_id(2)+x_id(1))/2);
    oxygen_c_gdl_340(k,1)   = data_340_340.CONTR{k}(10,(x_id(6)+x_id(5))/2);

    
    thermal_a_gdl_350(k,1)  = data_350_350.CONTR{k}(7,(x_id(2)+x_id(1))/2);
    thermal_c_gdl_350(k,1)  = data_350_350.CONTR{k}(7,(x_id(6)+x_id(5))/2);
    thermal_m_350(k,1)      = data_350_350.CONTR{k}(7,(x_id(4)+x_id(3))/2);
    
    ficks_a_gdl_350(k,1)    = data_350_350.CONTR{k}(8,(x_id(2)+x_id(1))/2);
    ficks_c_gdl_350(k,1)    = data_350_350.CONTR{k}(8,(x_id(6)+x_id(5))/2);
    ficks_m_350(k,1)        = data_350_350.CONTR{k}(8,(x_id(4)+x_id(3))/2);
   
    electro_a_gdl_350(k,1)  = data_350_350.CONTR{k}(9,(x_id(2)+x_id(1))/2);
    electro_c_gdl_350(k,1)  = data_350_350.CONTR{k}(9,(x_id(6)+x_id(5))/2);
    electro_m_350(k,1)      = data_350_350.CONTR{k}(9,(x_id(4)+x_id(3))/2);
    
    hydrogen_a_gdl_350(k,1) = data_340_340.CONTR{k}(10,(x_id(2)+x_id(1))/2);
    oxygen_c_gdl_350(k,1)   = data_340_340.CONTR{k}(10,(x_id(6)+x_id(5))/2);
    
    %Contributions temperature jump electrodes
    anode_Jq_340(k,1)        = data_340_340.CONTR{k}(4,x_id(2));
    anode_dufour_340(k,1)    = data_340_340.CONTR{k}(5,x_id(2));
    anode_peltier_340(k,1)   = data_340_340.CONTR{k}(6,x_id(2));
    anode_delta_T_340(k,1)   = data_340_340.SOL{k}.y(1,x_id(3))-data_340_340.SOL{k}.y(1,x_id(2));
    
    cathode_Jq_340(k,1)      = data_340_340.CONTR{k}(4,x_id(5));
    cathode_dufour_340(k,1)  = data_340_340.CONTR{k}(5,x_id(5));
    cathode_peltier_340(k,1) = data_340_340.CONTR{k}(6,x_id(5));
    cathode_delta_T_340(k,1) = data_340_340.SOL{k}.y(1,x_id(5))-data_340_340.SOL{k}.y(1,x_id(4));
    
    anode_Jq_350(k,1)        = data_350_350.CONTR{k}(4,x_id(2));
    anode_dufour_350(k,1)    = data_350_350.CONTR{k}(5,x_id(2));
    anode_peltier_350(k,1)   = data_350_350.CONTR{k}(6,x_id(2));
    anode_delta_T_350(k,1)   = data_350_350.SOL{k}.y(1,x_id(3))-data_350_350.SOL{k}.y(1,x_id(2));
    
    cathode_Jq_350(k,1)      = data_350_350.CONTR{k}(4,x_id(5));
    cathode_dufour_350(k,1)  = data_350_350.CONTR{k}(5,x_id(5));
    cathode_peltier_350(k,1) = data_350_350.CONTR{k}(6,x_id(5));
    cathode_delta_T_350(k,1) = data_350_350.SOL{k}.y(1,x_id(5))-data_350_350.SOL{k}.y(1,x_id(4));
end
contributions_heat_340 = [pel_a_gdl_340(end,:),pel_m_340(end,:),pel_c_gdl_340(end,:),...
                          fourier_a_gdl_340(end,:),fourier_m_340(end,:),fourier_c_gdl_340(end,:),...
                          hof_a_gdl_340(end,:),hof_m_340(end,:),hof_c_gdl_340(end,:),...
                          profile_Jq_340(end,:),profile_Jq_340(end,:),profile_Jq_340(end,:)];

contributions_heat_350 = [pel_a_gdl_350(end,:),pel_m_350(end,:),pel_c_gdl_350(end,:),...
                          fourier_a_gdl_350(end,:),fourier_m_350(end,:),fourier_c_gdl_350(end,:),...
                          hof_a_gdl_350(end,:),hof_m_350(end,:),hof_c_gdl_350(end,:),...
                          profile_Jq_350(end,:),profile_Jq_350(end,:),profile_Jq_350(end,:)];
                      
contributions_Jw_350   = [thermal_a_gdl_350(end,:),thermal_m_350(end,:),thermal_c_gdl_350(end,:),...
                          ficks_a_gdl_350(end,:),ficks_m_350(end,:),ficks_c_gdl_350(end,:),...
                          electro_a_gdl_350(end,:),electro_m_350(end,:),electro_c_gdl_350(end,:),...
                          profile_Jq_350(end,:),profile_Jq_350(end,:),profile_Jq_350(end,:)];



%% Plots for Cotnributions 340K
figure(1)
    subplot(2,3,1)
    box on
    hold on
    plot(cude,pel_a_gdl_340','k-.','LineWidth',2)
    plot(cude,fourier_a_gdl_340','r--','LineWidth',2)
    plot(cude,hof_a_gdl_340','b:','LineWidth',2)
    plot(cude,profile_Jq_340(:,(x_id(1)+x_id(2))/2)','k-','LineWidth',2)
    hold off
    legend('Peltier','Fourier','Dufour','J_q^{\prime}')
    title('J_q Contributions A GDL')
    lgd = legend;
    lgd.Location='SouthWest';
    xlabel('Current Density [A/m^2]')
    ylabel('Heat flux [J/(m^2 s)]')  
    
    subplot(2,3,2)
    box on
    hold on
    plot(cude,pel_m_340','k-.','LineWidth',2)
    plot(cude,fourier_m_340','r--','LineWidth',2)
    plot(cude,hof_m_340','b:','LineWidth',2)
    plot(cude,profile_Jq_340(:,(x_id(3)+x_id(4))/2)','k-','LineWidth',2)
    hold off
    legend('Peltier','Fourier','Dufour','J_q^{\prime}')
    title('J_q Contributions M')
    lgd = legend;
    lgd.Location='SouthWest';
    xlabel('Current Density [A/m^2]')
    ylabel('Heat flux [J/(m^2 s)]') 
    
    subplot(2,3,3)
    box on
    hold on
    plot(cude,pel_c_gdl_340','k-.','LineWidth',2)
    plot(cude,fourier_c_gdl_340','r--','LineWidth',2)
    plot(cude,hof_c_gdl_340','b:','LineWidth',2)
    plot(cude,profile_Jq_340(:,(x_id(5)+x_id(6))/2)','k-','LineWidth',2)
    hold off
    legend('Peltier','Fourier','Dufour','J_q^{\prime}')
    title('J_q Contributions C GDL')
    lgd = legend;
    lgd.Location='SouthWest';
    xlabel('Current Density [A/m^2]')
    ylabel('Heat flux [J/(m^2 s)]')  
    
    subplot(2,3,4)
    box on
    hold on
    plot(cude,hydrogen_a_gdl_340','r-.','LineWidth',2)
    plot(cude,thermal_a_gdl_340','k-.','LineWidth',2)
    plot(cude,ficks_a_gdl_340','r--','LineWidth',2)
    plot(cude,electro_a_gdl_340','b:','LineWidth',2)
    plot(cude,profile_Jw_340(:,(x_id(1)+x_id(2))/2),'k-','LineWidth',2)
    hold off
    legend('J_{H_2}','Therm. Diff.','Ficks Diff.','El.-Osm.','J_w')
    title('J_w Contributions A GDL')
    lgd = legend;
    lgd.Location='SouthWest';
    xlabel('Current Density [A/m^2]')
    ylabel('Mass flux [mol/(m^2 s)]')
    ylim([-0.4,0.3])
    
    subplot(2,3,5)
    box on
    hold on
    plot(cude,thermal_m_340','k-.','LineWidth',2)
    plot(cude,ficks_m_340','r--','LineWidth',2)
    plot(cude,electro_m_340','b:','LineWidth',2)
    plot(cude,profile_Jw_340(:,(x_id(3)+x_id(4))/2),'k-','LineWidth',2)
    hold off
    legend('Therm. Diff.','Ficks Diff.','El.-Osm.','J_w')
    title('J_w Contributions M')
    lgd = legend;
    lgd.Location='SouthWest';
    xlabel('Current Density [A/m^2]')
    ylabel('Mass flux [mol/(m^2 s)]')
    ylim([-0.4,0.3])
    subplot(2,3,6)
    box on
    hold on
    plot(cude,oxygen_c_gdl_340','r-.','LineWidth',2)
    plot(cude,thermal_c_gdl_340','k-.','LineWidth',2)
    plot(cude,ficks_c_gdl_340','r--','LineWidth',2)
    plot(cude,electro_c_gdl_340','b:','LineWidth',2)
    plot(cude,profile_Jw_340(:,(x_id(5)+x_id(6))/2),'k-','LineWidth',2)
    hold off
    legend('J_{O_2}','Therm. Diff.','Ficks Diff.','El.-Osm.','J_w')
    title('J_w Contributions C GDL')
    lgd = legend;
    lgd.Location='SouthWest';
    xlabel('Current Density [A/m^2]')
    ylabel('Mass flux [mol/(m^2 s)]') 
    ylim([-0.4,0.3])
%% Plots for Contributions 350K 
figure(2)
    subplot(2,3,1)
    box on
    hold on
    plot(cude,pel_a_gdl_350','k-.','LineWidth',2)
    plot(cude,fourier_a_gdl_350','r--','LineWidth',2)
    plot(cude,hof_a_gdl_350','b:','LineWidth',2)
    plot(cude,profile_Jq_350(:,(x_id(1)+x_id(2))/2)','k-','LineWidth',2)
    hold off
    legend('Peltier','Fourier','Dufour','J_q^{\prime}')
    title('J_q Contributions A GDL')
    lgd = legend;
    lgd.Location='best';
    xlabel('Current Density [A/m^2]')
    ylabel('Heat flux [J/(m^2 s)]')  
    
    subplot(2,3,2)
    box on
    hold on
    plot(cude,pel_m_350','k-.','LineWidth',2)
    plot(cude,fourier_m_350','r--','LineWidth',2)
    plot(cude,hof_m_350','b:','LineWidth',2)
    plot(cude,profile_Jq_350(:,(x_id(3)+x_id(4))/2)','k-','LineWidth',2)
    hold off
    legend('Peltier','Fourier','Dufour','J_q^{\prime}')
    title('J_q Contributions A GDL')
    lgd = legend;
    lgd.Location='best';
    xlabel('Current Density [A/m^2]')
    ylabel('Heat flux [J/(m^2 s)]') 
    
    subplot(2,3,3)
    box on
    hold on
    plot(cude,pel_c_gdl_350','k-.','LineWidth',2)
    plot(cude,fourier_c_gdl_350','r--','LineWidth',2)
    plot(cude,hof_c_gdl_350','b:','LineWidth',2)
    plot(cude,profile_Jq_350(:,(x_id(5)+x_id(6))/2)','k-','LineWidth',2)
    hold off
    legend('Peltier','Fourier','Dufour','J_q^{\prime}')
    title('J_q Contributions C GDL')
    lgd = legend;
    lgd.Location='best';
    xlabel('Current Density [A/m^2]')
    ylabel('Heat flux [J/(m^2 s)]')  
    
    subplot(2,3,4)
    box on
    hold on
    plot(cude,hydrogen_a_gdl_350','r-.','LineWidth',2)
    plot(cude,thermal_a_gdl_350','k-.','LineWidth',2)
    plot(cude,ficks_a_gdl_350','r--','LineWidth',2)
    plot(cude,electro_a_gdl_350','b:','LineWidth',2)
    plot(cude,profile_Jw_350(:,(x_id(1)+x_id(2))/2),'k-','LineWidth',2)
    hold off
    legend('J_{H_2}','Therm. Diff.','Diffusion','Electro-Osmosis','Effective water flux')
    title('J_w Contributions A GDL')
    lgd = legend;
    lgd.Location='best';
    xlabel('Current Density [A/m^2]')
    ylabel('Mass flux [mol/(m^2 s)]') 
    
    subplot(2,3,5)
    box on
    hold on
    plot(cude,thermal_m_350','k-.','LineWidth',2)
    plot(cude,ficks_m_350','r--','LineWidth',2)
    plot(cude,electro_m_350','b:','LineWidth',2)
    plot(cude,profile_Jw_350(:,(x_id(3)+x_id(4))/2),'k-','LineWidth',2)
    hold off
    legend('Therm. Diff.','Diffusion','Electro-Osmosis','Effective water flux')
    title('J_w Contributions M')
    lgd = legend;
    lgd.Location='best';
    xlabel('Current Density [A/m^2]')
    ylabel('Mass flux [mol/(m^2 s)]')
    
    subplot(2,3,6)
    box on
    hold on
    plot(cude,oxygen_c_gdl_350','r-.','LineWidth',2)
    plot(cude,thermal_c_gdl_350','k-.','LineWidth',2)
    plot(cude,ficks_c_gdl_350','r--','LineWidth',2)
    plot(cude,electro_c_gdl_350','b:','LineWidth',2)
    plot(cude,profile_Jw_350(:,(x_id(5)+x_id(6))/2),'k-','LineWidth',2)
    hold off
    legend('J_{O_2}','Therm. Diff.','Diffusion','Electro-Osmosis','Effective water flux')
    title('J_w Contributions C GDL')
    lgd = legend;
    lgd.Location='best';
    xlabel('Current Density [A/m^2]')
    ylabel('Mass flux [mol/(m^2 s)]') 
%% Electrode contributions   
figure(3)
    subplot(1,2,1)
    box on
    hold on
    plot(cude,anode_Jq_340','k-.','LineWidth',2)
    plot(cude,anode_dufour_340','r--','LineWidth',2)
    plot(cude,anode_peltier_340','b:','LineWidth',2)
    plot(cude,anode_delta_T_340','k-','LineWidth',2)
    hold off
    legend('J_q^{\prime}','Dufour','Peltier','\DeltaT Overall')
    title('\DeltaT Contributions Anode')
    lgd = legend;
    lgd.Location='NorthWest';
    xlabel('Current Density [A/m^2]')
    ylabel('\DeltaT [K]')  
    
    subplot(1,2,2)
    box on
    hold on
    plot(cude,cathode_Jq_340','k-.','LineWidth',2)
    plot(cude,cathode_dufour_340','r--','LineWidth',2)
    plot(cude,cathode_peltier_340','b:','LineWidth',2)
    plot(cude,cathode_delta_T_340','k-','LineWidth',2)
    hold off
    legend('J_q^{\prime}','Dufour','Peltier','\DeltaT Overall')
    title('\DeltaT Contributions Cathode')
    lgd = legend;
    lgd.Location='NorthWest';
    xlabel('Current Density [A/m^2]')
    ylabel('\DeltaT [K]')  
    
%     subplot(2,2,3)
%     box on
%     hold on
%     plot(cude,anode_Jq_350','k-.','LineWidth',2)
%     plot(cude,anode_dufour_350','r--','LineWidth',2)
%     plot(cude,anode_peltier_350','b:','LineWidth',2)
%     plot(cude,anode_delta_T_350','k-','LineWidth',2)
%     hold off
%     legend('J_q^{\prime}','Dufour','Peltier','\Delta T Overall')
%     title('T Jump Contributions Anode 350/350K')
%     lgd = legend;
%     lgd.Location='best';
%     xlabel('Current Density [A/m^2]')
%     ylabel('\Delta T [K]')  
%     
%     subplot(2,2,4)
%     box on
%     hold on
%     plot(cude,cathode_Jq_350','k-.','LineWidth',2)
%     plot(cude,cathode_dufour_350','r--','LineWidth',2)
%     plot(cude,cathode_peltier_350','b:','LineWidth',2)
%     plot(cude,cathode_delta_T_350','k-','LineWidth',2)
%     hold off
%     legend('J_q^{\prime}','Dufour','Peltier','\Delta T Overall')
%     title('T Jump Contributions Cathode 350/350K')
%     lgd = legend;
%     lgd.Location='best';
%     xlabel('Current Density [A/m^2]')
%     ylabel('\Delta T [K]') 