clear all
load('data_old_eq/data_base_parameters_BV_conc.mat')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%change data files here:
data_340_340=data_340_340_RH70_1atm;
data_350_350=data_350_350_RH70_1atm;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cude       = data_340_340.iv(1,:);
x_id       = data_340_340.X_ID{1};
IV_340_340 = data_340_340.iv;
IV_350_350 = data_350_350.iv;

for k=1:length(data_340_340.iv(1,:))
    rel_ee_a_gdl_340(1,k)=data_340_340.REL_EE{k}(1);
    rel_ee_a_340(1,k)=data_340_340.REL_EE{k}(2);
    rel_ee_m_340(1,k)=data_340_340.REL_EE{k}(3);
    rel_ee_c_340(1,k)=data_340_340.REL_EE{k}(4);
    rel_ee_c_gdl_340(1,k)=data_340_340.REL_EE{k}(5);

    rel_ee_a_gdl_350(1,k)=data_350_350.REL_EE{k}(1);
    rel_ee_a_350(1,k)=data_350_350.REL_EE{k}(2);
    rel_ee_m_350(1,k)=data_350_350.REL_EE{k}(3);
    rel_ee_c_350(1,k)=data_350_350.REL_EE{k}(4);
    rel_ee_c_gdl_350(1,k)=data_350_350.REL_EE{k}(5);
end

%%
figure(2)
hold on
plot(cude,rel_ee_a_gdl_340,'k-','LineWidth',2)
plot(cude,rel_ee_a_340,'r-','LineWidth',2)
plot(cude,rel_ee_m_340,'b-','LineWidth',2)
plot(cude,rel_ee_c_340,'r--','LineWidth',2)
plot(cude,rel_ee_c_gdl_340,'k--','LineWidth',2)
hold off
legend('A GDL', 'A', 'M', 'C', 'C GDL')
xlabel('Current Density [A/m^2]') 
ylabel('Relative entropy error difference [%]')
ylim([-100,100])
set(gca,'FontSize',20)
set(gcf,'Position',[1000,205,1200,1000])