%[data_340_340_RH70_1_5atm.iv,data_340_340_RH70_1_5atm.SOL,data_340_340_RH70_1_5atm.CONTR,data_340_340_RH70_1_5atm.X_ID,data_340_340_RH70_1_5atm.REL_EE]=main_bvp([340,340,0.7,0.7,1.5]);

SOL= data_340_340_RH70_1atm.SOL;
CONTR = data_340_340_RH70_1atm.CONTR;
iv = data_340_340_RH70_1atm.iv;
REL_EE = data_340_340_RH70_1atm.REL_EE;
X_ID = data_340_340_RH70_1atm.X_ID;
for k=1:length(iv(1,:))
    J_w_a(1,k)=SOL{k}.y(7,1);
    J_w_m(1,k)=SOL{k}.y(7,X_ID{k}(4));
    J_w_c(1,k)=SOL{k}.y(7,end);
    J_q_a(1,k)=SOL{k}.y(4,1);
    J_q_c(1,k)=SOL{k}.y(4,end);
    rel_ee_a_gdl(1,k)=REL_EE{k}(1);
    rel_ee_a(1,k)=REL_EE{k}(2);
    rel_ee_m(1,k)=REL_EE{k}(3);
    rel_ee_c(1,k)=REL_EE{k}(4);
    rel_ee_c_gdl(1,k)=REL_EE{k}(5);
    x_w_c(1,k)=SOL{k}.y(2,X_ID{k}(5));
    thermal(1,k)=CONTR{k}(7,X_ID{k}(4));
    ficks(1,k)=CONTR{k}(8,X_ID{k}(4));
    electro(1,k)=CONTR{k}(9,X_ID{k}(4));
end
figure(1)
hold on
plot(iv(1,:),thermal,'r:','LineWidth',2)
plot(iv(1,:),ficks,'r--','LineWidth',2)
plot(iv(1,:),electro,'r-','LineWidth',2)
plot(iv(1,:),J_w_m,'k-','LineWidth',2)
hold off
legend('Thermal Diffusion','Ficks Diffusion','Electro Osmosis','J_w')
xlabel('Current Density [A/m^2]') 
ylabel('Flux [mol m^{-2} s^{-1}]') 
lgd.Location='NorthWest';
set(gca,'FontSize',20)

figure(2)
hold on
plot(iv(1,:),rel_ee_a_gdl)
plot(iv(1,:),rel_ee_a)
plot(iv(1,:),rel_ee_m)
plot(iv(1,:),rel_ee_c)
plot(iv(1,:),rel_ee_c_gdl)
hold off

figure(3)
hold on
yyaxis left
plot(iv(1,:),J_w_a,'k-','LineWidth',2)
plot(iv(1,:), J_w_c,'k--','LineWidth',2)
yyaxis right
plot(iv(1,:),x_w_c,'r-','LineWidth',2)
hold off
legend('J_{w,anode}','J_{w,cathode}','x_{w,cathode}')
yyaxis left
ylabel('J_w [mol m^{-2} s^{-1}]') 
set(gca,'FontSize',20)
yyaxis right
ylabel('x_w [-]')
set(gca,'FontSize',20)
