function output=plot_profiles(SOL,cude,figure_name)
%Needs to be changed manually according to the main file:
L=[235 50.8 235].*10^-6;
Lsum=[0 cumsum(L)];

domains = [1 1 1;
    1 0 1;
    1 1 1;
    1 1 1;
    1 1 1;
    0 1 0;
    1 1 1];

unit_scale = [1 1 1 1 1 1 1];
quantity = {'T [K]', 'x_w [-]', '\phi [V]', 'J_q [J/(m^2 s)]', '\sigma', 'a_w [-]', 'J_w [mol/(m^2 s)]'};
Np=numel(SOL);
Neq=length(SOL{1}.y(:,1));

c = jet(Np);

figure('Name',figure_name)
for n = 1:Neq-1
    subplot(2,3,n)
    if n==6
        n=n+1;
    end
    box on
    hold on
    us = unit_scale(n);
    if n==2
        for k = 30:30:Np
            yyaxis left
            plot(SOL{k}.x*1e6, SOL{k}.y(n,:)*us,'-', 'Color', c(k,:), 'DisplayName', [num2str(cude(k)) ' A/m^2'])
            yyaxis right
            plot(SOL{k}.x*1e6, SOL{k}.y(n+4,:)*us,'--', 'Color', c(k,:), 'DisplayName', [num2str(cude(k)) ' A/m^2'])
        end
    else
        for k = [1 15:15:Np]
            plot(SOL{k}.x*1e6, SOL{k}.y(n,:)*us, 'Color', c(k,:), 'DisplayName', [num2str(cude(k)) ' A/m^2'])
        end
    end
    xlim([Lsum(find(domains(n,:),1,'first')) Lsum(find(domains(n,:),1,'last')+1)]*1e6)
    ylim(ylim)
    xlabel('x [\mum]')
    if n==2
        yyaxis left
        ylabel(quantity(n))
        yyaxis right
        ylabel(quantity(n+4))
    else
        ylabel(quantity(n))
    end
    
    
    for x = Lsum(2:end-1)
        l = line([x x]*1e6, ylim, 'Color', 'k');
        set(get(get(l, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off')
    end
end
legend('Position',[0.08447 0.4952 0.7993 0.0228],'NumColumns',11);
end