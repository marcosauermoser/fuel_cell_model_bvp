%[cude_plot,SOL]=main_bvp;
close all
clear all
clc
disp([340,340,0.7,0.7,1])
[data_340_340_RH70_1atm.iv,data_340_340_RH70_1atm.SOL,data_340_340_RH70_1atm.CONTR,data_340_340_RH70_1atm.X_ID,data_340_340_RH70_1atm.REL_EE]=main_bvp([340,340,0.7,0.7,1]);
disp([340,340,0.7,0.7,3])
[data_340_340_RH70_3atm.iv,data_340_340_RH70_3atm.SOL,data_340_340_RH70_3atm.CONTR,data_340_340_RH70_3atm.X_ID,data_340_340_RH70_3atm.REL_EE]=main_bvp([340,340,0.7,0.7,3]);
disp([350,350,0.7,0.7,1])
[data_350_350_RH70_1atm.iv,data_350_350_RH70_1atm.SOL,data_350_350_RH70_1atm.CONTR,data_350_350_RH70_1atm.X_ID,data_350_350_RH70_1atm.REL_EE]=main_bvp([350,350,0.7,0.7,1]);
disp([350,350,0.7,0.7,3])
[data_350_350_RH70_3atm.iv,data_350_350_RH70_3atm.SOL,data_350_350_RH70_3atm.CONTR,data_350_350_RH70_3atm.X_ID,data_350_350_RH70_3atm.REL_EE]=main_bvp([350,350,0.7,0.7,3]);
% disp([340,340,0.5,0.5,1])
% [data_340_340_RH50_1atm.iv,data_340_340_RH50_1atm.SOL,data_340_340_RH50_1atm.CONTR,data_340_340_RH50_1atm.X_ID,data_340_340_RH50_1atm.REL_EE]=main_bvp([340,340,0.5,0.5,1]);
% disp([340,340,0.5,0.5,3])
% [data_340_340_RH50_3atm.iv,data_340_340_RH50_3atm.SOL,data_340_340_RH50_3atm.CONTR,data_340_340_RH50_3atm.X_ID,data_340_340_RH50_3atm.REL_EE]=main_bvp([340,340,0.5,0.5,3]);
% disp([350,350,0.5,0.5,1])
% [data_350_350_RH50_1atm.iv,data_350_350_RH50_1atm.SOL,data_350_350_RH50_1atm.CONTR,data_350_350_RH50_1atm.X_ID,data_350_350_RH50_1atm.REL_EE]=main_bvp([350,350,0.5,0.5,1]);
% disp([350,350,0.5,0.5,3])
% [data_350_350_RH50_3atm.iv,data_350_350_RH50_3atm.SOL,data_350_350_RH50_3atm.CONTR,data_350_350_RH50_3atm.X_ID,data_350_350_RH50_3atm.REL_EE]=main_bvp([350,350,0.5,0.5,3]);
disp([340,345,0.7,0.7,1])
[data_340_345_RH70_1atm.iv,data_340_345_RH70_1atm.SOL,data_340_345_RH70_1atm.CONTR,data_340_345_RH70_1atm.X_ID,data_340_345_RH70_1atm.REL_EE]=main_bvp([340,345,0.7,0.7,1]);
disp([345,340,0.7,0.7,1])
[data_345_340_RH70_1atm.iv,data_345_340_RH70_1atm.SOL,data_345_340_RH70_1atm.CONTR,data_345_340_RH70_1atm.X_ID,data_345_340_RH70_1atm.REL_EE]=main_bvp([345,340,0.7,0.7,1]);
disp([335,340,0.7,0.7,1])
[data_335_340_RH70_1atm.iv,data_335_340_RH70_1atm.SOL,data_335_340_RH70_1atm.CONTR,data_335_340_RH70_1atm.X_ID,data_335_340_RH70_1atm.REL_EE]=main_bvp([335,340,0.7,0.7,1]);
disp([340,335,0.7,0.7,1])
[data_340_335_RH70_1atm.iv,data_340_335_RH70_1atm.SOL,data_340_335_RH70_1atm.CONTR,data_340_335_RH70_1atm.X_ID,data_340_335_RH70_1atm.REL_EE]=main_bvp([340,335,0.7,0.7,1]);
