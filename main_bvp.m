function [iv,SOL,CONTR,x_id,REL_EE]=main_bvp(input,figure_options)
%Defines all parameters
%1=Corrected BV, 0= Non-Corrected BV
option_BV = 1;
dxwdx_eq = 1; %1 is new equation, 0 is old equation.
%General
F   = 96485;       % Farraday's constant [C/mol]
R   = 8.314;       % Gas constant [J/(mol K)]
j_0 = 2.5*10^(-3); % Cathode exhcange current density [A/m^2]

dry_air=0.21;
%Inlet and initial conditions
switch nargin
    case 1
        T_l  = input(1); %Inlet temperature Anode [K]
        T_r  = input(2); %Inlet temperature Cathode [K]
        RH_l = input(3); %Relative humidity Anode
        RH_r = input(4); %Relative humidity Cathode
        p    = input(5);
        plot_on = 0;
    case 0
        T_l  = 340; %Inlet temperature Anode [K]
        T_r  = 340; %Inlet temperature Cathode [K]
        RH_l = 0.7; %Relative humidity Anode
        RH_r = 0.7; %Relative humidity Cathode
        p    = 1;
        plot_on = 0;
    case 2
        T_l  = input(1); %Inlet temperature Anode [K]
        T_r  = input(2); %Inlet temperature Cathode [K]
        RH_l = input(3); %Relative humidity Anode
        RH_r = input(4); %Relative humidity Cathode
        p    = input(5);
        figure_name = figure_options;
        plot_on     = 1;
end


p_sat  = @(T) 1/1.01325 * 10^(5.11564-1687.537/((T-273.15)+230.17));
x_w_l  = RH_l*p_sat(T_l)/p;
x_w_r  = RH_r*p_sat(T_r)/p;
guess_phi_a_gdl = 0;
guess_phi_m     = -0.242289541341171;
guess_phi_c_gdl = 1.18275714770588;
%Porosity times tourtosity of GDL
epsilon_tau=0.14;
%epsilon_tau = 0.044; %Sigracet S25BC from GDL material data paper
%System dimensions [m]
L=[235 50.8 235].*10^-6;
%Molar weight of molecules [g/mol]
M_w  = 18;
M_O2 = 32;
M_H2 = 2;
M_N2 = 28;
%Parameters for diffusion coefficient calculations
E_w        = 3.166;  %[Å]
epsilon_w  = 78.166; %[K]
E_O2       = 3.433;  %[Å]
epsilon_O2 = 113;    %[K]
E_H2       = 2.915;  %[Å]
epsilon_H2 = 38;     %[K]
E_N2       = 3.667;  %[Å]
epsilon_N2 = 99.8;   %[K]
%Heat capacities [J/(mol K)]
cp_w   = 34; %water vapor
cp_w_m = 75; %liquid water
cp_O2  = 29;
cp_H2  = 29;
%Standard enthalpies [J/mol]
H_0_w   = -242000;
H_0_w_m = -285000; %liquid water
H_0_O2  = 1000;
H_0_H2  = 1000;
H_H2    = @(T) H_0_H2 + cp_H2.*(T-298);
H_w     = @(T) H_0_w + cp_w.*(T-298); 
H_wl    = @(T) H_0_w_m + cp_w_m.*(T-298);
H_O2    = @(T) H_0_O2 + cp_O2.*(T-298);
%Standard enthalpies [J/(mol K)]
S_0_w   = 189;
S_0_w_m = 70; %liquid water
S_0_O2  = 205;
S_0_H2  = 135;
S_H2    = @(T,x_H2) S_0_H2 + cp_H2.*log(T./298) - R.*log(x_H2);
S_w     = @(T,x_w) S_0_w + cp_w.*log(T./298) - R.*log(x_w); 
S_wl    = @(T,x_wl) S_0_w_m + cp_w_m.*log(T./298) - R.*log(x_wl);
S_O2    = @(T,x_O2) S_0_O2 + cp_O2.*log(T./298) - R.*log(x_O2);
%transference numbers [-]
t_w_a_gdl = 0 ;
t_w_m     = 1.2;
t_w_al    = t_w_a_gdl;
t_w_ar    = t_w_m;
t_w_cr    = t_w_a_gdl + 0.5;
t_w_cl    = t_w_m;
t_w_c_gdl = t_w_a_gdl+0.5;
%Entropies of electrons [J/(mol K)]
S_e_a_gdl = -2;
S_e_c_gdl = -2;
S_e_al    = -2;
S_e_cr    = -2;
%Thermal conductivity 
lambda_a_gdl = 0.38; % Sigracet 28BC  [W/(m K)]
%lambda_a_gdl= 1.6;
lambda_a     = 1000; % reference data [W/(m^2 K)]
lambda_c     = 1000; % reference data [W/(m2^2 K)]
lambda_c_gdl = 0.38; % Sigracet 28BC  [W/(m K)]
%lambda_c_gdl= 1.6;
%electric resistance [Ohm m^2]
r_a_gdl = 0.00021;     % Sigracet 28BC  [Ohm m]
r_a     = 7.2*10^(-6); % reference data [Ohm m^2]
r_c     = 7.2*10^(-6); % reference data [Ohm m^2]
r_c_gdl = 0.00021;     % Sigracet 28BC  [Ohm m]
%Membrane material parameters
M_m         = 1.1;    % Dry molar mass of membrane [kg/mol]
roh_m       = 1968.5; % Dry density of Nafion 212
l_w_m       = @(a_w) 1.067e-14.*exp(34.18.*a_w) + 1.402.*exp(1.958.*a_w); %Weber Model
dlw_mda_w_m = @(a_w) 1.067e-14.*34.18.*exp(34.18.*a_w)+1.402.*1.958.*exp(1.958.*a_w); %Weber Model
%l_w_m        = 0.043+17.81.*a_w-39.85.*a_w.^2+36.*a_w.^3; %Springer Model
%dlw_mda_w_m  = 17.81-2*39.85.*a_w+3*36.*a_w.^2; %Springer Model
D_w_m       = @(T, a_w)10.^(-10).*exp(2416.*(1/303-1./T)).*(2.563 - 0.33.*l_w_m(a_w) +...
                          0.0264.*l_w_m(a_w).^2 -6.71.*10.^(-4).*l_w_m(a_w).^3);
lambda_m    = @(a_w) 0.177 + 3.7.*10.^(-3).*l_w_m(a_w);
cude = [0:10:100,200:100:14000];
% AUXILIARY FUNCTIONS
iff = @(cond,a,b) cond.*a + ~cond.*b; % vectorized ternary operator
%Create Mesh
Lsum = [0 cumsum(L)];
Nd   = length(L);
x    = interp1(0:Nd, Lsum, linspace(0, Nd, Nd*30+1));
x    = sort([x Lsum(2:end-1)]);

%Prepare the solver:
sol = bvpinit(x, @yinit);
options = bvpset('Vectorized','on','NMax', 1e3, 'RelTol', 1e-4, 'AbsTol',1e-6);

%Parameter Sweep
V   = zeros(size(cude));
SOL = cell(size(cude));
Np  = length(cude);
Neq = length(sol.y(:,1));
for k = 1:Np
    cude_k=cude(k)
    sol = bvp4c(@odefun, @(ya,yb) bcfun(ya,yb,cude_k), sol, options);
    SOL{k} = sol;
    V(k)   = SOL{k}.y(3,end);    
    
end
iv=[cude;V];
Nref = 2; % number of refinements for smoother curve plotting
domains = [1 1 1;
           1 0 1;
           1 1 1;
           1 1 1;
           1 1 1;
           0 1 0;
           1 1 1];
shift = 1e-10;
x_id=cell(size(cude));

for k = 1:Np
    x = [];
    temp=0;   
    for m = 1:Nd
        xa = find(SOL{k}.x==Lsum(m  ), 1, 'last' );
        xb = find(SOL{k}.x==Lsum(m+1), 1, 'first');
        N  = xb-xa;
        N_ref_total=121;
        % grid refinement old version used N_ref_total=N*2^Nref+1
        interp = interp1(linspace(0,1,N+1), SOL{k}.x(xa:xb), linspace(shift, 1-shift, N_ref_total ));
        x    = [x interp];
        x_id{k}=[x_id{k} temp+1 m*length(interp)] ;
        temp=m*length(interp);
        % fill solution on inactive domains with NaN
        SOL{k}.y(~domains(:,m),xa:xb) = NaN;
     end
    [SOL{k}.y, SOL{k}.yp] = deval(SOL{k}, x);
    SOL{k}.x = x;
end
%Calculate contributions using the refined grid
CONTR = cell(size(cude));

for k=1:Np
contr        = ones(10,length(SOL{k}.x));
contr(4:10,:) = NaN;
    for m=1:Nd
        x_id_a=x_id{k}(m*2-1);
        x_id_b=x_id{k}(m*2);
        switch m
            case 1
                [transfer,peltier,fourier]=contribution_a_gdl(SOL{k}.y(1,x_id_a:x_id_b),...
                                           SOL{k}.y(2,x_id_a:x_id_b),SOL{k}.y(7,x_id_a:x_id_b),SOL{k}.y(4,x_id_a:x_id_b),cude(k));
                contr(1,x_id_a:x_id_b)=transfer;
                contr(2,x_id_a:x_id_b)=peltier;
                contr(3,x_id_a:x_id_b)=fourier;
                [contr_J_q,contr_dufour,contr_peltier] = jump_contribution_anode(SOL{k}.y(1,x_id_b),...
                                                        SOL{k}.y(1,x_id_b+1),SOL{k}.y(2,x_id_b),SOL{k}.y(6,x_id_b+1),...
                                                        SOL{k}.y(4,x_id_b),SOL{k}.y(4,x_id_b+1),SOL{k}.y(7,x_id_b),cude(k));
                contr(4,x_id_b) = contr_J_q;
                contr(5,x_id_b) = contr_dufour;
                contr(6,x_id_b) = contr_peltier;
                [hydrogen,thermal,ficks,electro]=J_w_contribution_a_gdl(SOL{k}.y(1,x_id_a:x_id_b),...
                                        SOL{k}.y(2,x_id_a:x_id_b),SOL{k}.yp(1,x_id_a:x_id_b),SOL{k}.yp(2,x_id_a:x_id_b),cude(k));
                contr(7,x_id_a:x_id_b) = thermal;
                contr(8,x_id_a:x_id_b) = ficks;
                contr(9,x_id_a:x_id_b) = electro;
                contr(10,x_id_a:x_id_b)= hydrogen;
            case 2
                [transfer,peltier,fourier]=contribution_m(SOL{k}.y(1,x_id_a:x_id_b),...
                                           SOL{k}.y(6,x_id_a:x_id_b),SOL{k}.y(7,x_id_a:x_id_b),SOL{k}.y(4,x_id_a:x_id_b),cude(k));
                contr(1,x_id_a:x_id_b)=transfer;
                contr(2,x_id_a:x_id_b)=peltier;
                contr(3,x_id_a:x_id_b)=fourier;
                [thermal,ficks,electro]=J_w_contribution_m(SOL{k}.y(1,x_id_a:x_id_b),...
                                        SOL{k}.y(6,x_id_a:x_id_b),SOL{k}.yp(1,x_id_a:x_id_b),cude(k));
                contr(7,x_id_a:x_id_b) = thermal;
                contr(8,x_id_a:x_id_b) = ficks;
                contr(9,x_id_a:x_id_b) = electro;
                contr(10,x_id_a:x_id_b)= NaN;
            case 3
                [transfer,peltier,fourier]=contribution_c_gdl(SOL{k}.y(1,x_id_a:x_id_b),...
                                           SOL{k}.y(2,x_id_a:x_id_b),SOL{k}.y(7,x_id_a:x_id_b),SOL{k}.y(4,x_id_a:x_id_b),cude(k));
                contr(1,x_id_a:x_id_b)=transfer;
                contr(2,x_id_a:x_id_b)=peltier;
                contr(3,x_id_a:x_id_b)=fourier;
                [contr_J_q,contr_dufour,contr_peltier] = jump_contribution_cathode(SOL{k}.y(1,x_id_a-1),...
                                                        SOL{k}.y(1,x_id_a),SOL{k}.y(6,x_id_a-1),SOL{k}.y(2,x_id_a),...
                                                        SOL{k}.y(4,x_id_a-1),SOL{k}.y(4,x_id_a),SOL{k}.y(7,x_id_a-1),cude(k));
                contr(4,x_id_a) = contr_J_q;
                contr(5,x_id_a) = contr_dufour;
                contr(6,x_id_a) = contr_peltier;
                [oxygen,thermal,ficks,electro]=J_w_contribution_c_gdl(SOL{k}.y(1,x_id_a:x_id_b),...
                                        SOL{k}.y(2,x_id_a:x_id_b),SOL{k}.yp(1,x_id_a:x_id_b),SOL{k}.yp(2,x_id_a:x_id_b),cude(k));
                contr(7,x_id_a:x_id_b) = thermal;
                contr(8,x_id_a:x_id_b) = ficks;
                contr(9,x_id_a:x_id_b) = electro;
                contr(10,x_id_a:x_id_b)= oxygen;
                
        end
        
        CONTR{k}=contr;
    end
end
%Calculate entropy error using the refined grid data
REL_EE = cell(size(cude));
for k=1:Np
    rel_ee=zeros(1,5);
    %Anode GDL
    delta_sensible=SOL{k}.y(4,x_id{k}(2))/SOL{k}.y(1,x_id{k}(2))-SOL{k}.y(4,x_id{k}(1))/SOL{k}.y(1,x_id{k}(1));
    delta_w=SOL{k}.y(7,x_id{k}(1))*(S_w(SOL{k}.y(1,x_id{k}(2)),SOL{k}.y(2,x_id{k}(2)))-S_w(SOL{k}.y(1,x_id{k}(1)),SOL{k}.y(2,x_id{k}(1))));
    delta_H2=cude(k)/(2*F)*(S_H2(SOL{k}.y(1,x_id{k}(2)),(1-SOL{k}.y(2,x_id{k}(2))))-S_H2(SOL{k}.y(1,x_id{k}(1)),(1-SOL{k}.y(2,x_id{k}(1)))));
    e_j_a_gdl=delta_sensible+delta_w+delta_H2;
    
    %Anode
    delta_sensible=SOL{k}.y(4,x_id{k}(3))/SOL{k}.y(1,x_id{k}(3))-SOL{k}.y(4,x_id{k}(2))/SOL{k}.y(1,x_id{k}(2));
    delta_w=SOL{k}.y(7,x_id{k}(2))*(S_wl(SOL{k}.y(1,x_id{k}(3)),SOL{k}.y(6,x_id{k}(3)))-S_w(SOL{k}.y(1,x_id{k}(2)),SOL{k}.y(2,x_id{k}(2))));
    delta_H2=-(cude(k)/(2*F))*S_H2(SOL{k}.y(1,x_id{k}(2)),(1-SOL{k}.y(2,x_id{k}(2))));
    e_j_a=delta_sensible+delta_w+delta_H2;
    
    %Membrane
    delta_sensible=SOL{k}.y(4,x_id{k}(4))/SOL{k}.y(1,x_id{k}(4))-SOL{k}.y(4,x_id{k}(3))/SOL{k}.y(1,x_id{k}(3));
    delta_w=SOL{k}.y(7,x_id{k}(3))*(S_wl(SOL{k}.y(1,x_id{k}(4)),SOL{k}.y(6,x_id{k}(4)))-S_wl(SOL{k}.y(1,x_id{k}(3)),SOL{k}.y(6,x_id{k}(3))));
    e_j_m=delta_sensible+delta_w;
    
    %Cathode
    delta_sensible=SOL{k}.y(4,x_id{k}(5))/SOL{k}.y(1,x_id{k}(5))-SOL{k}.y(4,x_id{k}(4))/SOL{k}.y(1,x_id{k}(4));
    delta_w=SOL{k}.y(7,x_id{k}(5))*S_w(SOL{k}.y(1,x_id{k}(5)),SOL{k}.y(2,x_id{k}(5)))-SOL{k}.y(7,x_id{k}(4))*S_wl(SOL{k}.y(1,x_id{k}(4)),SOL{k}.y(6,x_id{k}(4)));
    delta_O2=-cude(k)/(4*F)*S_O2(SOL{k}.y(1,x_id{k}(5)),dry_air.*(1-SOL{k}.y(2,x_id{k}(5))));
    e_j_c=delta_sensible+delta_w+delta_O2;

    %Cathode GDL
    delta_sensible=SOL{k}.y(4,x_id{k}(6))/SOL{k}.y(1,x_id{k}(6))-SOL{k}.y(4,x_id{k}(5))/SOL{k}.y(1,x_id{k}(5));
    delta_w=SOL{k}.y(7,x_id{k}(5))*(S_w(SOL{k}.y(1,x_id{k}(6)),SOL{k}.y(2,x_id{k}(6)))-S_w(SOL{k}.y(1,x_id{k}(5)),SOL{k}.y(2,x_id{k}(5))));
    delta_O2=-cude(k)/(4*F)*(S_O2(SOL{k}.y(1,x_id{k}(6)),dry_air.*(1-SOL{k}.y(2,x_id{k}(6))))-S_O2(SOL{k}.y(1,x_id{k}(5)),dry_air.*(1-SOL{k}.y(2,x_id{k}(5)))));
    e_j_c_gdl=delta_sensible+delta_w+delta_O2;

    rel_ee(1)=((SOL{k}.y(5,x_id{k}(2))-SOL{k}.y(5,x_id{k}(1)))-e_j_a_gdl)/e_j_a_gdl*100;
    rel_ee(2)=((SOL{k}.y(5,x_id{k}(3))-SOL{k}.y(5,x_id{k}(2)))-e_j_a)/e_j_a*100;
    rel_ee(3)=((SOL{k}.y(5,x_id{k}(4))-SOL{k}.y(5,x_id{k}(3)))-e_j_m)/e_j_m*100;
    rel_ee(4)=((SOL{k}.y(5,x_id{k}(5))-SOL{k}.y(5,x_id{k}(4)))-e_j_c)/e_j_c*100;
    rel_ee(5)=((SOL{k}.y(5,x_id{k}(6))-SOL{k}.y(5,x_id{k}(5)))-e_j_c_gdl)/e_j_c_gdl*100;
    REL_EE{k}=rel_ee;
    
    
    
    
    
end

% PLOT SOLUTION

unit_scale = [1 1 1 1 1 1 1];
quantity = {'T [K]', 'x_w [-]', '\phi [V]', 'J_q [J/(m^2 s)]', '\sigma', 'a_w [-]', 'J_w [mol/(m^2 s)]'};
c = jet(Np);
if plot_on==1
    figure('Name',figure_name)
    for n = 1:Neq
        subplot(3,3,n)
        box on
        hold on
        us = unit_scale(n);
        for k = 1:Np
            plot(SOL{k}.x*1e6, SOL{k}.y(n,:)*us, 'Color', c(k,:), 'DisplayName', [num2str(cude(k)) 'A/m^2'])
        end
        xlim([Lsum(find(domains(n,:),1,'first')) Lsum(find(domains(n,:),1,'last')+1)]*1e6)
        ylim(ylim)
        xlabel('x [um]')
        ylabel(quantity(n))
        for x = Lsum(2:end-1)
            l = line([x x]*1e6, ylim, 'Color', 'k');
            set(get(get(l, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off')
        end
    end
end

function dydx = odefun(x, y, subdomain)
    T     = y(1,:); x_w = y(2,:);
    phi   = y(3,:); J_q = y(4,:);
    sigma = y(5,:); a_w = y(6,:);
    J_w   = y(7,:);

    init = zeros(size(x));
    dTdx     = init; dxwdx = init;
    dphidx   = init; dJqdx = init;
    dsigmadx = init; dawdx = init;
    dJ_wdx   = init;
    switch subdomain
        case 1
            J_H2       = cude_k/(2*F);
            J_D        = (J_w./x_w-J_H2./(1-x_w)).*x_w;
            q_w        = -T.*S_w(T,x_w);
            pi_a_gdl   = -T.*(0.5*S_H2(T,(1-x_w))+S_e_a_gdl);
            D_w_a_gdl  = epsilon_tau.*calc_D_AB("w","H2",T);
            
            dTdx  = -1/lambda_a_gdl .*(J_q-q_w.*(J_D-t_w_a_gdl.*cude_k./F)-pi_a_gdl.*cude_k./F);
            dxwdx = -(q_w .* x_w)./(R.*T.^2) .*dTdx - 1./D_w_a_gdl.*(J_D - t_w_a_gdl.*cude_k./F);
            dphidx= -pi_a_gdl./(F.*T).*dTdx - (t_w_a_gdl.*R.*T)./(F.*x_w).*dxwdx-r_a_gdl.*cude_k;
            dJqdx = -cude_k.*dphidx - (J_H2.*cp_H2 + J_w.*cp_w).*dTdx;
            dsigmadx = -((J_q./(T.^2)).*dTdx + cude_k./T .*dphidx + ((J_D.*R)./x_w).*dxwdx); 
            
        case 2
            q_w   = -T.*S_wl(T,a_w);
            pi_m  = 13.*T;
            r_m         = (exp(1268.*(1/303-1./T)).*(0.5139.*l_w_m(a_w)-0.326)).^(-1); %electric resistance [Ohm m^2]
            dTdx     = (-1./lambda_m(a_w) .*(J_q-q_w.*(J_w-t_w_m.*cude_k./F)-pi_m.*cude_k./F));
            dawdx    = (-(q_w.*l_w_m(a_w))./(dlw_mda_w_m(a_w).*R.*T.^2).*dTdx - ...
                        (J_w - t_w_m.*cude_k./F).*M_m./(dlw_mda_w_m(a_w).*roh_m.*D_w_m(T, a_w)));
            dphidx   = (-pi_m./(F.*T).*dTdx -(t_w_m.*R.*T)./(F.*a_w).*dawdx- r_m.*cude_k);
            dJqdx    = (-cude_k.*dphidx - J_w.*cp_w_m.*dTdx);
            dsigmadx = (-((J_q./(T.^2)).*dTdx + cude_k./T .*dphidx + J_w./(R.*a_w).*dawdx));
            
        case 3
            x_O2       = dry_air.*(1-x_w);
            q_w        = -T.*S_w(T,x_w);
            pi_c_gdl   = T.*(0.25*S_O2(T,x_O2)-S_e_c_gdl-t_w_c_gdl*S_w(T,x_w));
            J_O2       = -cude_k/(4*F);
            J_D        = (J_w./x_w-J_O2./x_O2).*x_w;
            D_O2_c_gdl = epsilon_tau.*calc_D_AB("O2","N2",T);
            D_w_O2_c_gdl = epsilon_tau.*calc_D_AB("w","O2",T);
            dTdx     = -1/lambda_c_gdl .*(J_q-q_w.*J_D-pi_c_gdl.*cude_k./F);
            if dxwdx_eq == 1
                dxwdx = -(q_w .* x_w)./(R.*T.^2) .*dTdx - 1./D_w_O2_c_gdl.*(J_D - t_w_c_gdl.*cude_k./F);
            else
                dxwdx    = -cude_k./(4.*F.*D_O2_c_gdl);
            end
            dphidx   = -pi_c_gdl./(F.*T).*dTdx - r_c_gdl.*cude_k;
            dJqdx    = -cude_k.*dphidx - (J_w.*cp_w + J_O2.*cp_O2).*dTdx;
            dsigmadx = -((J_q./(T.^2)).*dTdx + cude_k./T .*dphidx + ((J_D.*R)./x_w).*dxwdx);
    end
    dydx = [dTdx; dxwdx;
            dphidx; dJqdx;
            dsigmadx; dawdx;
            dJ_wdx];

end

function y0 = yinit(x, subdomain)

    T = (T_r + T_l)/2;
    x_w = iff(subdomain==1, x_w_l, iff(subdomain==3, x_w_r, 0));
    phi = iff(subdomain==1,guess_phi_a_gdl, iff(subdomain==2, guess_phi_m, iff(subdomain==3, guess_phi_c_gdl,0)));
    
    mu_w_m_entrance = H_w(T_l) - T_l.*S_w(T_l,x_w_l);
    guess_a_w  = exp((mu_w_m_entrance-H_wl(T_l))./(R.*T_l) ...
                  +S_0_w_m/R + cp_w_m/R.*log(T_l/298));
    a_w = iff(subdomain==2, guess_a_w,0);
    
    y0 = [T; x_w; phi; 0; 0; a_w; 0];
end

function res = bcfun(ya, yb,cude_k)

    res = ya(:); % homogeneous BC everywhere by default
    %Calculation of Anode:
    J_H2 = cude_k./(2.*F);
    q_w_al          = -yb(1,1).*S_w(yb(1,1),yb(2,1));
    q_w_ar          = -ya(1,2).*S_wl(ya(1,2),ya(6,2));
    pi_al           = -yb(1,1).*(0.5.*S_H2(yb(1,1),yb(2,1)) + S_e_al);
    pi_ar           = 13.*ya(1,2);
    T_as            = yb(1,1) - 1./lambda_a.*(yb(4,1)-q_w_al.*(yb(7,1) - t_w_al.*cude_k./F)-pi_al.*cude_k./F);
    guess_eta_a     = ((2.*R.*T_as)./F) .*log((cude_k+1e-12)./j_0);
    if option_BV==1
        eta_a       = fzero(@butler_volmer_concentration,guess_eta_a,[],T_as,"anode",(1-yb(2,1)),cude_k);
    else
        eta_a       = fzero(@butler_volmer,guess_eta_a,[],T_as,"anode",cude_k);
    end
    delta_phi_a_eff = -eta_a -(pi_al./(yb(1,1).*F)).*(T_as-yb(1,1)) - (pi_ar./(ya(1,2).*F)).*(ya(1,2)-T_as) - r_a.*cude_k;
    delta_G_a       = -H_H2(T_as) + T_as.*S_H2(T_as,(1-yb(2,1)));

    delta_Jq_a      = cude_k.*yb(3,1)+J_H2.*H_H2(yb(1,1))+yb(7,1).*H_w(yb(1,1))-cude_k.*ya(3,2)-ya(7,2).*H_wl(ya(1,2));
    delta_T_a       = -1./lambda_a.*(yb(4,1)+ya(4,2) + q_w_al.*(yb(7,1)-t_w_a_gdl.*cude_k./F)...
                      + q_w_ar.*(ya(7,2)-t_w_m.*cude_k./F) - (cude_k./F).*(pi_al+pi_ar));
    delta_phi_a     = delta_phi_a_eff - delta_G_a./(2.*F);
    delta_sigma_a   = ya(4,2).*(1./ya(1,2) -1./T_as) + yb(4,1).*(1./T_as -1./yb(1,1)) - cude_k./T_as .*delta_phi_a_eff;

    %Calculation of Cathode:
    J_O2    = -cude_k./(4.*F);
    q_w_cl  = -yb(1,2).*S_wl(yb(1,2),yb(6,2));
    q_w_cr  = -ya(1,3).*S_w(ya(1,3),ya(2,3));
    pi_cr   = ya(1,3).*(0.25.*S_O2(ya(1,3),dry_air.*(1-ya(2,3))) - S_e_cr - t_w_cr.*S_w(ya(1,3),ya(2,3)));
    pi_cl   = 13.*yb(1,2);
    T_cs    = yb(1,2) - 1./lambda_c.*(yb(4,2)-q_w_cl.*(yb(7,2) - t_w_cl.*cude_k./F)-pi_cl.*cude_k./F);
    guess_eta_c = ((2.*R.*T_cs)./F) .*log((cude_k+1e-12)./j_0);
    if option_BV==1
        eta_c       = fzero(@butler_volmer_concentration,guess_eta_c,[],T_cs,"cathode",dry_air.*(1-ya(2,3)),cude_k);
    else
        eta_c       = fzero(@butler_volmer,guess_eta_c,[],T_cs,"cathode",cude_k);
    end
    delta_phi_c_eff = -eta_c - (pi_cl./(yb(1,2).*F)).*(T_cs - yb(1,2)) - (pi_cr./(ya(1,3).*F)).*(yb(1,3) - T_cs) - r_c.*cude_k;
    delta_G_c       = H_w(ya(1,3)) - T_cs.*S_w(ya(1,3),ya(2,3)) - (H_O2(ya(1,3)) - T_cs.*S_O2(ya(1,3),dry_air.*(1-ya(2,3))))./2;


    delta_Jq_c    = cude_k.*yb(3,2) + yb(7,2).*H_wl(yb(1,2)) -cude_k.*ya(3,3) ...
                    - ya(7,3).*H_w(ya(1,3)) - J_O2.*H_O2(dry_air.*(1-ya(2,3)));
    delta_T_c     = -1./lambda_c.*(yb(4,2)+ya(4,3) + q_w_cl.*(yb(7,2)-t_w_m.*cude_k./F)...
                    + q_w_cr.*(ya(7,3)-t_w_c_gdl.*cude_k./F) - (cude_k./F).*(pi_cl+pi_cr));
    delta_phi_c   = delta_phi_c_eff - delta_G_c ./(2.*F);
    delta_sigma_c = yb(4,2).*(1./yb(1,2) - 1./T_cs) + ya(4,3).*(1./T_cs - 1./ya(1,3)) - cude_k./T_cs.*delta_phi_c_eff;
    
    mu_w_m_entrance = H_w(yb(1,1)) - yb(1,1).*S_w(yb(1,1),yb(2,1));
    a_w_entrance    = exp((mu_w_m_entrance-H_wl(ya(1,2)))./(R.*ya(1,2)) ...
                      +S_0_w_m/R + cp_w_m/R.*log(ya(1,2)/298));
    
    mu_w_cl = H_wl(yb(1,2)) - yb(1,2).*(S_0_w_m+cp_w_m.*log(yb(1,2)./298)-R.*log(yb(6,2)));
    mu_w_cr = mu_w_cl;
    S_w_cr  = (H_w(ya(1,3))-mu_w_cr)./ya(1,3);
    x_w_c_r = exp((1./R).*(S_0_w+cp_w .* log(ya(1,3)./298) - S_w_cr));
    %Anode GDL:
    res(0*Neq+1) = ya(1,1)-T_l;
    res(0*Neq+2) = ya(2,1)-x_w_l;
    res(0*Neq+3) = ya(3,1); %potential 0 at a gdl inlet
    res(0*Neq+4) = yb(4,1)-ya(4,2)+delta_Jq_a;
    res(0*Neq+5) = ya(5,1);
    %res(0*Neq+6)= ;
    res(0*Neq+7) = yb(7,2)-ya(7,1);

    %Membrane
    res(1*Neq+1) = ya(1,2)-yb(1,1)-delta_T_a; 
    %res(0*Neq+2)= ;
    res(1*Neq+3) = ya(3,2)-yb(3,1)-delta_phi_a;
    res(1*Neq+4) = yb(4,2)-ya(4,3)+delta_Jq_c;
    res(1*Neq+5) = ya(5,2)-yb(5,1)-delta_sigma_a;
    res(1*Neq+6) = ya(6,2)-a_w_entrance;
    res(1*Neq+7) =-ya(7,3)+yb(7,2)+cude_k./(2.*F);

    %Cathode GDL
    res(2*Neq+1) = yb(1,3)-T_r;
    res(2*Neq+2) = yb(2,3)-x_w_r;
    res(2*Neq+3) = ya(3,3)-yb(3,2)-delta_phi_c;
    res(2*Neq+4) = ya(1,3)-yb(1,2)-delta_T_c;
    res(2*Neq+5) = ya(5,3)-yb(5,2)-delta_sigma_c;
    %res(0*Neq+6)= ;
    res(2*Neq+7) = ya(2,3)-x_w_c_r;
end




function D_AB_corrected = calc_D_AB(type1,type2,T)
    if type1=="w"
        M_A       = M_w;
        E_A       = E_w;
        epsilon_A = epsilon_w;
    elseif type1=="O2"
        M_A       = M_O2;
        E_A       = E_O2;
        epsilon_A = epsilon_O2;
    elseif type1=="H2"
        M_A       = M_H2;
        E_A       = E_H2;
        epsilon_A = epsilon_H2;
    elseif type1=="N2"
        M_A       = M_N2;
        E_A       = E_N2;
        epsilon_A = epsilon_N2;
    else
        error('Wrong type of molecule')
    end
    if type2=="w"
        M_B       = M_w;
        E_B       = E_w;
        epsilon_B = epsilon_w;
    elseif type2=="O2"
        M_B       = M_O2;
        E_B       = E_O2;
        epsilon_B = epsilon_O2;
    elseif type2=="H2"
        M_B       = M_H2;
        E_B       = E_H2;
        epsilon_B = epsilon_H2;
    elseif type2=="N2"
        M_B       = M_N2;
        E_B       = E_H2;
        epsilon_B = epsilon_H2;
    else
        error('Wrong type of molecule')
    end
    E_AB = 0.5.*(E_A + E_B);
    epsilon_AB = sqrt(epsilon_A*epsilon_B);
    T_ad = T./epsilon_AB;
    omega_AB = 1.06036./T_ad.^0.15610 + 0.193./(exp(0.47635.*T_ad)) + 1.03587./(exp(1.52996.*T_ad)) + ...
               1.76474./(exp(3.89411.*T_ad));
    D_AB = 1.8583.*10.^(-7).*sqrt(1./M_A + 1./M_B).*(T.^1.5)./(p.*E_AB.^2.*omega_AB);
    D_AB_corrected =D_AB.*(p .*1.01325.*10.^5)./(R.*T);
end
function res = butler_volmer_concentration(eta_c,T,type,x,cude_k)
    %Parameter defintion
    alpha_a_cathode = 0.5;
    alpha_c_cathode = 0.5;
    alpha_a_anode   = 0.5;
    alpha_c_anode   = 0.5;
    %Electrode Pt Surface area
    T_celsius=T-273.15;
    EPSA_c = 0.02396*T_celsius^2-5.958*T_celsius+429.3;
    EPSA_a = 0.0009915*T_celsius^2-0.5201*T_celsius+55.94;
    %reference pressures
    p_O2_ref = 0.986923;
    p_H2_ref = 0.986923;
    %Overpotential calculation for either cathode or anode
    if type=="cathode"
        p_O2=p*x;
        j_0_c = EPSA_c*3e-5*(p_O2/p_O2_ref)^(0.001678*T);
        eta = (R*T)/(alpha_a_cathode*2*F)*log(cude_k/j_0_c +exp((-alpha_c_cathode*2*F*eta_c)/(R*T)));

    elseif type=="anode"
        p_H2=p*x;
        j_0_a = EPSA_a*10*(p_H2/p_H2_ref)^0.5;
        eta = (R*T)/(alpha_a_anode*2*F)*log(cude_k/j_0_a +exp((-alpha_c_anode*2*F*eta_c)/(R*T)));
    else
        error('Wrong type of electrode!');
    end  

    %Residual for fzero or fsolve
    res = eta_c-eta;
end
function res = butler_volmer(eta_c,T,type,cude_k)
    %Parameter defintion
    alpha_a_cathode = 0.5;
    alpha_c_cathode = 0.5;
    alpha_a_anode   = 0.5;
    alpha_c_anode   = 0.5;
    T_celsius = T-273.15;
    EPSA_c = 0.02396*T_celsius^2-5.958*T_celsius+429.3;
    EPSA_a = 0.0009915*T_celsius^2-0.5201*T_celsius+55.94;
    %Overpotential calculation for either cathode or anode
    if type=="cathode"
        j_0_c = EPSA_c*3e-5;
        eta = (R*T)/(alpha_a_cathode*2*F)*log(cude_k/j_0_c ...
            +exp((-alpha_c_cathode*2*F*eta_c)/(R*T)));
    elseif type=="anode"
        j_0_a = EPSA_a*10;
        eta = (R*T)/(alpha_a_anode*2*F)*log(cude_k/j_0_a +exp((-alpha_c_anode*2*F*eta_c)/(R*T)));
    else
        error('Wrong type of electrode!');
    end  

    %Residual for fzero or fsolve
    res = eta_c-eta;
end

function [transfer,peltier,fourier]=contribution_a_gdl(T,x_w,J_w,J_q,cude_k)
%Calculate inter-diffusion flux and entropies
J_H2     = cude_k/(2*F);
J_D      = (J_w./x_w-J_H2./(1-x_w)).*x_w;
q_w      = -T.*S_w(T,x_w);
pi_a_gdl = -T.*(0.5*S_H2(T,(1-x_w))+S_e_a_gdl);
transfer = q_w.*(J_D-t_w_a_gdl.*cude_k./F);
peltier  = pi_a_gdl.*cude_k./F;
fourier  = J_q-transfer-peltier;
end

function [transfer,peltier,fourier]=contribution_m(T,a_w,J_w,J_q,cude_k)
%Calculate inter-diffusion flux and entropies
q_w      = -T.*S_wl(T,a_w);
pi_m     = 13.*T;
transfer = q_w.*(J_w-t_w_m.*cude_k./F);
peltier  = pi_m.*cude_k./F;
fourier  = J_q-transfer-peltier;
end

function [transfer,peltier,fourier]=contribution_c_gdl(T,x_w,J_w,J_q,cude_k)
%Calculate Entropies
J_w_c=J_w + cude_k./(2*F);
J_O2 = -cude_k/(4*F);
J_D  = (J_w_c./x_w-J_O2./(1-x_w)).*x_w;
q_w      = -T.*S_w(T,x_w);
pi_c_gdl = -T.*(0.25*S_O2(T,dry_air.*(1-x_w))+S_e_c_gdl-t_w_c_gdl*S_w(T,x_w));
transfer = q_w.*J_D;
peltier  = pi_c_gdl.*cude_k./F;
fourier  = J_q-transfer-peltier;
end
function [contr_J_q,contr_dufour,contr_peltier]=...
    jump_contribution_cathode(T_cl,T_cr,a_w,x_w,J_q_cl,J_q_cr,J_w_cl,cude_k)

J_w_cr = J_w_cl + cude_k./(2.*F);
q_w_cl = -T_cl.*S_wl(T_cl,a_w);
q_w_cr = -T_cr.*S_w(T_cr,x_w);
pi_cr  = T_cr.*(0.25.*S_O2(T_cr,dry_air.*(1-x_w)) - S_e_cr - t_w_cr.*S_w(T_cr,x_w));
pi_cl  = 13.*T_cl;
contr_J_q     = -1./lambda_c.*(J_q_cr+J_q_cl);
contr_dufour  = 1./lambda_c.*(q_w_cr.*J_w_cr+q_w_cl.*J_w_cl)...
                    -cude_k./(lambda_c.*F).*(q_w_cl.*t_w_cl + q_w_cr.*t_w_cr);
contr_peltier = cude_k./(lambda_c.*F).*(pi_cr + pi_cl);
end
function [contr_J_q,contr_dufour,contr_peltier]=...
    jump_contribution_anode(T_al,T_ar,x_w,a_w,J_q_al,J_q_ar,J_w_al,cude_k)
J_w_ar  = J_w_al;
q_w_al  = -T_al.*S_w(T_al,x_w);
q_w_ar  = -T_ar.*S_wl(T_ar,a_w);
pi_al   = -T_al.*(0.5.*S_H2(T_al,(1-x_w)) + S_e_al);
pi_ar   = 13.*T_ar;

contr_J_q     = -1./lambda_a.*(J_q_al+J_q_ar);
contr_dufour  = 1./lambda_a.*(q_w_al.*J_w_al+q_w_ar.*J_w_ar)...
                -cude_k./(lambda_a.*F).*(q_w_al.*t_w_al + q_w_ar.*t_w_ar);
contr_peltier = cude_k./(lambda_a.*F).*(pi_al + pi_ar);

end
function [thermal,ficks,electro]=J_w_contribution_m(T,a_w,dTdx,cude_k)
n_x     = length(T);
l_a_gdl = L(1);
l_m     = L(2);
dlw_mdx = (dss020(l_a_gdl,(l_a_gdl+l_m),n_x,l_w_m(a_w),1));
q_w     = -T.*S_wl(T,a_w);
thermal = -(q_w.*D_w_m(T, a_w).*l_w_m(a_w).*roh_m)./(R.*M_m.*T.^2).*dTdx;
ficks   = -(D_w_m(T, a_w).*roh_m)./(M_m).*dlw_mdx;
electro = t_w_m.*cude_k./F.*ones(1,length(T));
end
function [hydrogen,thermal,ficks,electro]=J_w_contribution_a_gdl(T,x_w,dTdx,dxwdx,cude_k)
D_w_a_gdl = epsilon_tau.*calc_D_AB("w","H2",T);
J_H2      = cude_k/(2*F);
q_w       = - T.*S_w(T,x_w);
hydrogen  = J_H2.*x_w./(1-x_w);
thermal   = - D_w_a_gdl.*(q_w .* x_w)./(R.*T.^2) .*dTdx;
ficks     = - D_w_a_gdl.*dxwdx;
electro   = t_w_a_gdl.*cude_k./F*ones(1,length(T));
end
function [oxygen,thermal,ficks,electro]=J_w_contribution_c_gdl(T,x_w,dTdx,dxwdx,cude_k)
if dxwdx_eq ==1
    D_w_c_gdl = epsilon_tau.*calc_D_AB("w","O2",T);
    J_O2      = -cude_k/(4*F);
    q_w       = - T.*S_w(T,x_w);
    oxygen    = J_O2.*x_w./(dry_air.*(1-x_w));
    thermal   = - D_w_c_gdl.*(q_w .* x_w)./(R.*T.^2) .*dTdx;
    ficks     = - D_w_c_gdl.*dxwdx;
    electro   = t_w_c_gdl.*cude_k./F*ones(1,length(T));
else
    oxygen  = NaN.*ones(1,length(T));
    thermal = NaN.*ones(1,length(T));
    ficks   = NaN.*ones(1,length(T));
    electro = NaN.*ones(1,length(T));
end
end
function ux=dss020(xl,xu,n,u,v)
%Routine dss020 implements fourth order directional differencing
%
%Intended for first order hyperbolic pde systems, e.g.
%
%       U +v*U  = 0
%        t    x
%
%Positive v for medium flowing in direction of higher x.
%
%Based on a routine with the same name in a book by W.E.Schiesser,
%The numerical method of lines, page 135.
dx    = (xu-xl)/(n-1);
rdx = 1/(12*dx);
if (v > 0)
        ux(n) = rdx*(  3*u(n-4)-16*u(n-3)+36*u(n-2)-48*u(n-1)+25*u(n));
        ux(1) = rdx*(-25*u(1)+48*u(2)-36*u(3)+16*u(4)- 3*u(5));
        ux(2) = rdx*(- 3*u(1)-10*u(2)+18*u(3)- 6*u(4)+ 1*u(5));
        ux(3) = rdx*(  1*u(1)- 8*u(2)+ 0*u(3)+ 8*u(4)- 1*u(5));
        for j=4:(n-1),
                ux(j) = rdx*(- 1*u(j-3)+ 6*u(j-2)-18*u(j-1)+10*u(j)+3*u(j+1));
        end
else

        ux(n)   = rdx*(  3*u(n-4)-16*u(n-3)+36*u(n-2)-48*u(n-1)+25*u(n));
        ux(n-1) = rdx*( -1*u(n-4)+ 6*u(n-3)-18*u(n-2)+10*u(n-1)+ 3*u(n));
        ux(n-2) = rdx*(  1*u(n-4)- 8*u(n-3)+ 0*u(n-2)+ 8*u(n-1)- 1*u(n));

        ux(1) = rdx*(-25*u(1)+48*u(2)-36*u(3)+16*u(4)- 3*u(5));
        for j=2:(n-3),
                ux(j) = rdx*(- 1*u(j-1)+ 6*u(j)-18*u(j+1)+10*u(j+2)+3*u(j+3));
        end
end
end
end